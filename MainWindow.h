#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSet>

class PlayerSheetWidget;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void onNewGameTriggered() const;
    void restartGame() const;
    void onPbStartClicked();
    void setPlayers(const QStringList & players);

    Ui::MainWindow *ui;
    QSet<PlayerSheetWidget*> _playerSheetWidgets;
};
#endif // MAINWINDOW_H
