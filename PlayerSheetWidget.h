#ifndef PLAYERSHEETWIDGET_H
#define PLAYERSHEETWIDGET_H

#include <QWidget>
#include <QHash>

#include "PlayerSheet.h"

namespace Ui {
class PlayerSheetWidget;
}

class QSpinBox;
class QLabel;

class PlayerSheetWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerSheetWidget(const QString & name, QWidget *parent = nullptr);
    ~PlayerSheetWidget();

    void reset();

private:
    void onScoreChanged(PlayerSheet::Color color);
    void onNumberStoneChanged();
    void updateTotalScore();

    void onPlaceValueChanged(int value) const;
    void onDistrictValueChanged(int value) const;
    void onStonesValueChanged(int value) const;

    Ui::PlayerSheetWidget * _ui;
    PlayerSheet * _playerSheet;

    QHash<PlayerSheet::Color,QSpinBox*> _districtSpinboxs;
    QHash<PlayerSheet::Color,QSpinBox*> _placeSpinboxs;
    QHash<PlayerSheet::Color,QLabel*> _scores;
};

#endif // PLAYERSHEETWIDGET_H
