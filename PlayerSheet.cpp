#include "PlayerSheet.h"
#include <QDebug>

PlayerSheet::PlayerSheet(QObject * parent)
    :  QObject(parent)
{
    _scoreForPlaces[Color::Red] = 2;
    _scoreForPlaces[Color::Blue] = 1;
    _scoreForPlaces[Color::Green] = 3;
    _scoreForPlaces[Color::Yellow] = 2;
    _scoreForPlaces[Color::Purple] = 2;
}

void PlayerSheet::setPlaceNumber(PlayerSheet::Color color, int number)
{
    if(_places.value(color,0) != number)
    {
        _places[color] = number;
        emit scoreChanged(color);
    }
}

void PlayerSheet::setScoreDistrict(PlayerSheet::Color color, int number)
{
    if(_scoreDistrict.value(color,0) != number)
    {
        _scoreDistrict[color] = number;
        emit scoreChanged(color);
    }
}

void PlayerSheet::setStones(int number)
{
    if(_stones != number)
    {
        _stones = number;
        emit stoneChanged();
    }
}

int PlayerSheet::getScoreDistrict(PlayerSheet::Color color) const
{
    return _scoreDistrict.value(color,0);
}

int PlayerSheet::getPlaceNumber(PlayerSheet::Color color) const
{
    return _places.value(color,0);
}

int PlayerSheet::getTotalScore(PlayerSheet::Color color) const
{
        return _places.value(color,0) * _scoreForPlaces.value(color,0) * _scoreDistrict.value(color,0);
}

int PlayerSheet::getTotalScore() const
{
    int score = 0;
    for (auto color : {Color::Blue, Color::Green, Color::Purple, Color::Red, Color::Yellow})
    {
        score += getTotalScore(color);
    }
    score += _stones;

    return score;
}

void PlayerSheet::reset()
{
    setStones(0);
    for(Color color : {Color::Blue, Color::Green, Color::Purple, Color::Red, Color::Yellow})
    {
        setPlaceNumber(color,0);
        setScoreDistrict(color,0);
    }
}
