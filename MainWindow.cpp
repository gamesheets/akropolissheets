#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "PlayerSheetWidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pbStart, &QPushButton::clicked, this, &MainWindow::onPbStartClicked);
    connect(ui->actionRestartGame, &QAction::triggered, this, &MainWindow::restartGame);
    connect(ui->actionNewGame, &QAction::triggered, this, &MainWindow::onNewGameTriggered);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onNewGameTriggered()const
{
    ui->stackedWidget->setCurrentWidget(ui->pagePlayers);
}

void MainWindow::restartGame()const
{
    for(auto playerSheetWidget : _playerSheetWidgets)
    {
        playerSheetWidget->reset();
    }
}

void MainWindow::onPbStartClicked()
{
    QStringList players;
    for(QLineEdit * lePlayer : {ui->lePlayer1, ui->lePlayer2, ui->lePlayer3, ui->lePlayer4})
    {
        QString player = lePlayer->text();
        if(!player.isEmpty())
        {
            players.append(player);
        }
    }
    setPlayers(players);
    ui->stackedWidget->setCurrentWidget(ui->pageSheets);
}

void MainWindow::setPlayers(const QStringList &players)
{
    qDeleteAll(_playerSheetWidgets);
    _playerSheetWidgets.clear();

    //On supprimme les vertical lines présentes dans le layout
    while (QLayoutItem *item = ui->playerLayout->takeAt(0))
    {
        if (QWidget *widget = item->widget())
        {
            delete widget;
        }
        delete item;
    }

    for(auto player : players)
    {
        //On ajoute une séparation verticale si le layout contient déjà des widgets
        if(ui->playerLayout->count()>0)
        {
            auto line = new QFrame();
            line->setFrameShape(QFrame::VLine);
            line->setFrameShadow(QFrame::Sunken);
            ui->playerLayout->addWidget(line);
        }
        PlayerSheetWidget *sheet = new PlayerSheetWidget(player,this);
        ui->playerLayout->addWidget(sheet);
        _playerSheetWidgets.insert(sheet);
    }
}

