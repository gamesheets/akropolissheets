#ifndef PLAYERSHEET_H
#define PLAYERSHEET_H

#include <QHash>
#include <QObject>

class QString;

class PlayerSheet : public QObject
{
    Q_OBJECT
public:
    enum Color
    {
        Red,
        Blue,
        Purple,
        Yellow,
        Green,
    };
    PlayerSheet(QObject * parent = Q_NULLPTR);

    void setPlaceNumber(Color color, int number);
    void setScoreDistrict(Color color, int number);
    void setStones(int number);

    int getScoreDistrict(Color color) const;
    int getPlaceNumber(Color color) const;
    int getStones() const;

    int getTotalScore(Color color) const;
    int getTotalScore() const;

    void reset();
signals:
    void scoreChanged(Color color);
    void stoneChanged();

private:
    QHash<Color,int> _places;
    QHash<Color,int> _scoreDistrict;
    QHash<Color,int> _scoreForPlaces;
    int _stones = 0;
};

#endif // PLAYERSHEET_H
