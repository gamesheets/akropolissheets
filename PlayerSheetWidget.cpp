#include "PlayerSheetWidget.h"
#include "ui_PlayerSheetWidget.h"

PlayerSheetWidget::PlayerSheetWidget(const QString & name, QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::PlayerSheetWidget),
    _playerSheet(new PlayerSheet(this))
{
    _ui->setupUi(this);
    _ui->lblName->setText(name);

    connect(_playerSheet, &PlayerSheet::scoreChanged, this, &PlayerSheetWidget::onScoreChanged);
    connect(_playerSheet, &PlayerSheet::stoneChanged, this, &PlayerSheetWidget::onNumberStoneChanged);

    _placeSpinboxs.insert(PlayerSheet::Color::Blue, _ui->sbBluePlace);
    _districtSpinboxs.insert(PlayerSheet::Color::Blue, _ui->sbBlueDistrict);
    _scores.insert(PlayerSheet::Color::Blue, _ui->lblBlueScore);

    _placeSpinboxs.insert(PlayerSheet::Color::Green, _ui->sbGreenPlace);
    _districtSpinboxs.insert(PlayerSheet::Color::Green, _ui->sbGreenDistrict);
    _scores.insert(PlayerSheet::Color::Green, _ui->lblGreenScore);

    _placeSpinboxs.insert(PlayerSheet::Color::Yellow, _ui->sbYellowPlace);
    _districtSpinboxs.insert(PlayerSheet::Color::Yellow, _ui->sbYellowDistrict);
    _scores.insert(PlayerSheet::Color::Yellow, _ui->lblYellowScore);

    _placeSpinboxs.insert(PlayerSheet::Color::Red, _ui->sbRedPlace);
    _districtSpinboxs.insert(PlayerSheet::Color::Red, _ui->sbRedDistrict);
    _scores.insert(PlayerSheet::Color::Red, _ui->lblRedScore);

    _placeSpinboxs.insert(PlayerSheet::Color::Purple, _ui->sbPurplePlace);
    _districtSpinboxs.insert(PlayerSheet::Color::Purple, _ui->sbPurpleDistrict);
    _scores.insert(PlayerSheet::Color::Purple, _ui->lblPurpleScore);

    for(auto spinBox : _placeSpinboxs)
    {
        connect(spinBox, &QSpinBox::valueChanged, this, &PlayerSheetWidget::onPlaceValueChanged);
    }

    for(auto spinBox : _districtSpinboxs)
    {
        connect(spinBox, &QSpinBox::valueChanged, this, &PlayerSheetWidget::onDistrictValueChanged);
    }

    connect(_ui->sbStones, &QSpinBox::valueChanged, this, &PlayerSheetWidget::onStonesValueChanged);
}

PlayerSheetWidget::~PlayerSheetWidget()
{
    delete _ui;
}

void PlayerSheetWidget::reset()
{
    _ui->sbStones->setValue(0);
    for(auto sb : _districtSpinboxs)
    {
        sb->setValue(0);
    }
    for(auto sb : _placeSpinboxs)
    {
        sb->setValue(0);
    }
}

void PlayerSheetWidget::onScoreChanged(PlayerSheet::Color color)
{
    QLabel * labelColorScore = _scores.value(color, Q_NULLPTR);
    if(labelColorScore)
        labelColorScore->setText(QString::number(_playerSheet->getTotalScore(color)));
    updateTotalScore();
}

void PlayerSheetWidget::onNumberStoneChanged()
{
    updateTotalScore();
}

void PlayerSheetWidget::updateTotalScore()
{
    _ui->lblTotalScore->setText(QString::number(_playerSheet->getTotalScore()));
}

void PlayerSheetWidget::onPlaceValueChanged(int value) const
{
    auto object = sender();
    QSpinBox* spinBox = dynamic_cast<QSpinBox*>(object);
    if(spinBox)
    {
        _playerSheet->setPlaceNumber(_placeSpinboxs.key(spinBox), value);
    }
}

void PlayerSheetWidget::onDistrictValueChanged(int value) const
{
    auto object = sender();
    QSpinBox* spinBox = dynamic_cast<QSpinBox*>(object);
    if(spinBox)
    {
        _playerSheet->setScoreDistrict(_districtSpinboxs.key(spinBox), value);
    }
}

void PlayerSheetWidget::onStonesValueChanged(int value) const
{
    _playerSheet->setStones(value);
}
